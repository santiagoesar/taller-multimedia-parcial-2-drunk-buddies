﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaInstrucciones2 : MonoBehaviour {

	public GameObject PantallaInstrucciones3;

	public void GoToPantallaIntrucciones3(){

		//Busco la pantalla PantallaInicio
		GameObject PantallaInstrucciones2 = GameObject.Find("Pantallas/PantallaInstrucciones2");
		GameObject.Destroy(PantallaInstrucciones2);

		//Busco al GameObject Pantallas
		GameObject Pantallas = GameObject.Find("Pantallas");
		GameObject pantallaCopia = GameObject.Instantiate(PantallaInstrucciones3, Pantallas.transform).gameObject;
		pantallaCopia.name = "PantallaInstrucciones3";
	}
}
