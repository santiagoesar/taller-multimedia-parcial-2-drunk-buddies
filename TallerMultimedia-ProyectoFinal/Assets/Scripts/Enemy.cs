using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	// Variables para gestionar el radio de visión y velocidad
	public float visionRadius;
	public float speed;

	public AudioSource audioenemy;

	Vector3 lastPosition;

	// Variable para guardar al jugador
	GameObject Ebrio;

	// Variable para guardar la posición inicial
	Vector3 initialPosition;

	void Start () {

		// Recuperamos al jugador gracias al Tag
		Ebrio = GameObject.FindGameObjectWithTag("Player");

		// Guardamos nuestra posición inicial
		initialPosition = transform.position;
		lastPosition = transform.position;

	}

	void Update () {

		// Por defecto nuestro objetivo siempre será nuestra posición inicial
		Vector3 target = initialPosition;
		lastPosition = transform.position;

		// Pero si la distancia hasta el jugador es menor que el radio de visión el objetivo será él
		float dist = Vector3.Distance(Ebrio.transform.position, transform.position);
		if (dist < visionRadius) {
			target = Ebrio.transform.position;

			// Guardamos la posicion actual del personaje secundario
			lastPosition = transform.position;
		}

		// Finalmente movemos al enemigo en dirección a su target
		float fixedSpeed = speed*Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, target, fixedSpeed);

		// Detectamos hacia adonde se esta moviendo le personaje

		// Y podemos debugearlo con una línea
		Debug.DrawLine(transform.position, target, Color.green);


		float diffX = transform.position.x - lastPosition.x;
		float diffY = transform.position.y - lastPosition.y;

		// El personaje secundario se mueve a la derecha
		if (diffX > 0 && Mathf.Abs (diffX) > 0.001) {
			animator.SetBool ("StayIdle", false);
			animator.SetBool ("GoRight", true);

		// El personaje secundario se mueve a la izquierda
		} else if (diffX < 0 && Mathf.Abs (diffX) > 0.001) {
			animator.SetBool ("StayIdle", false);
			animator.SetBool ("GoRight", false);

		} else {
			animator.SetBool ("StayIdle", true);
		}
	}

	public Animator animator;

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player") {
			audioenemy.Play ();
		}
	}
		
	// Podemos dibujar el radio de visión sobre la escena dibujando una esfera
    void OnDrawGizmos() {

		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, visionRadius);

    }

}