﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Props : MonoBehaviour {

	public int value = 10;
	public GameObject explosion;

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Player") {
			// explotar si se especifica
			if (explosion != null) {
				Instantiate (explosion, transform.position, Quaternion.identity);
			}
			// destruir después de recolectar
			Destroy (gameObject);
		}
	}
}
