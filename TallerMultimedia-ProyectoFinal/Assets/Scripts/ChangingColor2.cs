﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangingColor2 : MonoBehaviour {

	public GameObject panel;

	public SpriteRenderer hair;

	public Color negro;
	public Color moreno;
	public Color trigueno;
	public Color amarillo;
	public Color blanco;

	public int whatColor = 1;

	void Update (){

		if (whatColor == 1) {
			hair.color = negro;
		} else if (whatColor == 2){
			hair.color = moreno;
		} else if (whatColor == 3){
			hair.color = trigueno;
		} else if (whatColor == 4){
			hair.color = amarillo;
		} else if (whatColor == 5){
			hair.color = blanco;
		}
	}


	public void OpenPanel (){

		panel.SetActive (true);
	}

	public void ClosePanel (){

		panel.SetActive (false);
	}

	public void ChangeHairNegro(){
		whatColor = 1;
	}

	public void ChangeHairMoreno(){
		whatColor = 2;
	}

	public void ChangeHairTrigueno(){
		whatColor = 3;
	}

	public void ChangeHairAmarillo(){
		whatColor = 4;
	}

	public void ChangeHairBlanco(){
		whatColor = 5;
	}

}
