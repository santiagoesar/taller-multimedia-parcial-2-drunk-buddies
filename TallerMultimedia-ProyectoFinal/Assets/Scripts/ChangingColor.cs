﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangingColor : MonoBehaviour {

	public GameObject panel;

	public SpriteRenderer head;

	public Color negro;
	public Color moreno;
	public Color trigueno;
	public Color amarillo;
	public Color blanco;

	public int whatColor = 1;

	void Update (){
	
		if (whatColor == 1) {
			head.color = negro;
		} else if (whatColor == 2){
			head.color = moreno;
		} else if (whatColor == 3){
			head.color = trigueno;
		} else if (whatColor == 4){
			head.color = amarillo;
		} else if (whatColor == 5){
			head.color = blanco;
		}
	}


	public void OpenPanel (){
	
		panel.SetActive (true);
	}

	public void ClosePanel (){

		panel.SetActive (false);
	}

	public void ChangeHeadNegro(){
		whatColor = 1;
	}

	public void ChangeHeadMoreno(){
		whatColor = 2;
	}

	public void ChangeHeadTrigueno(){
		whatColor = 3;
	}

	public void ChangeHeadAmarillo(){
		whatColor = 4;
	}

	public void ChangeHeadBlanco(){
		whatColor = 5;
	}

}
