﻿using UnityEngine;
using System.Collections;

public class Strober: MonoBehaviour{
	
	Light Spotlight;
	public float minWaitTime;
	public float maxWaitTime;

	void Start(){
		Spotlight = GetComponent<Light> ();
		StartCoroutine (Flashing ());
	}

	IEnumerator Flashing(){
		while (true) {
			yield return new WaitForSeconds (Random.Range (minWaitTime, maxWaitTime));
			Spotlight.enabled = ! Spotlight.enabled;
		}
	}
}



