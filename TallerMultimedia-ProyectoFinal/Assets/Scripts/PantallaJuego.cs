﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaJuego : MonoBehaviour {

	// UI
	public GameObject PantallaGameOver;
	public GameObject PantallaWinner;
	public GameObject PantallaPausa;
	Slider SliderBorrachera;
	Slider SliderFarra;
	Text textTime;
	Text Text;


	// State
	int time;
	int text;

	GameManager manager;

	void Start (){

		InvokeRepeating("Tiempo", 0.0f, 1.0f);
		
		manager = GameObject.Find("GameManager").GetComponent<GameManager> ();

		SliderBorrachera = GameObject.Find ("Pantallas/PantallaJuego/SliderBorrachera/Slider").GetComponent<Slider> ();

		SliderFarra = GameObject.Find ("Pantallas/PantallaJuego/SliderFarra/Slider").GetComponent<Slider> ();

		textTime = GameObject.Find ("Pantallas/PantallaJuego/PanelTime/TextTime").GetComponent<Text> ();
		time = 0;

		Text = GameObject.Find ("Pantallas/PantallaJuego/Contador de Monedas/Text").GetComponent<Text> ();
		text = 0;

	}

	void Update (){
		
		SliderBorrachera.value = manager.SliderBorrachera;
		SliderFarra.value = manager.SliderFarra;
	}

	void Tiempo() {

		// Actualizamos el tiempo
		time = time + 1;

		if (time < 10) {
			textTime.text = "00:0" + time.ToString ();

		} else if (time < 60) {
			textTime.text = "00:" + time.ToString ();
		
		} else {
			int mins = time / 60;
			int segs = time % 60;
			textTime.text = mins.ToString() + ":" + segs.ToString ();
		}

		// Si el tiempo del nivel se acabo
		if(time == 120) {

			//Busco al GameObject Pantallas
			GameObject Pantallas = GameObject.Find("Pantallas");
			GameObject pantallaCopy = GameObject.Instantiate(PantallaGameOver, Pantallas.transform).gameObject;
			pantallaCopy.name = "PantallaGameOver";
			GameObject.Destroy(this);

			// Destruyo la camara que no esta adentro del sobrio
			GameObject otherCamera = GameObject.Find ("CameraOutside");
			otherCamera.SetActive (true);

			// Busco el level para destruirlo
			GameObject level = GameObject.Find("Level");
			GameObject.Destroy (level);

		}

		if (SliderFarra.value == 100) {
			
			//Busco al GameObject Pantallas
			GameObject Pantallas = GameObject.Find("Pantallas");
			GameObject pantallaCopy = GameObject.Instantiate(PantallaGameOver, Pantallas.transform).gameObject;
			pantallaCopy.name = "PantallaGameOver";
			GameObject.Destroy(this);

			// Destruyo la camara que no esta adentro del sobrio
			GameObject otherCamera = GameObject.Find ("CameraOutside");
			otherCamera.SetActive (true);

			// Busco el level para destruirlo
			GameObject level = GameObject.Find("Level");
			GameObject.Destroy (level);
		}

		if (SliderBorrachera.value == 0) {

			//Busco al GameObject Pantallas
			GameObject Pantallas = GameObject.Find("Pantallas");
			GameObject pantallaCopy = GameObject.Instantiate(PantallaGameOver, Pantallas.transform).gameObject;
			pantallaCopy.name = "PantallaGameOver";
			GameObject.Destroy(this);

			// Destruyo la camara que no esta adentro del sobrio
			GameObject otherCamera = GameObject.Find ("CameraOutside");
			otherCamera.SetActive (true);

			// Busco el level para destruirlo
			GameObject level = GameObject.Find("Level");
			GameObject.Destroy (level);
		}
	}

	public void GoToPausa(){

		//Busco la pantalla PantallaMundo
		GameObject otherPantallaJuego = GameObject.Find("Pantallas/PantallaJuego");
		otherPantallaJuego.SetActive (false);

		// Muestro la pantalla Juego
		GameObject Pantallas = GameObject.Find("Pantallas");
		GameObject pantallaCopia = GameObject.Instantiate(PantallaPausa, Pantallas.transform).gameObject;
		pantallaCopia.name = "PantallaPausa";

		// Destruyo la camara que no esta adentro del sobrio
		GameObject otherCamera = GameObject.Find ("CameraOutside");
		otherCamera.SetActive (true);

		GameObject Level = GameObject.Find ("Level");
		Level.SetActive (false);

		GameObject Sobrio = GameObject.Find ("Sobrio");
		Sobrio.SetActive (false);
	}
			
}
