﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrunkPlayer : MonoBehaviour {

    Camera gameCamera;

	void Start() {
		gameCamera = GameObject.Find ("Level/Game/Ebrio/Camera").GetComponent<Camera>();
	}

    void FixedUpdate() {
		Ray ray = gameCamera.ScreenPointToRay(Input.mousePosition);
        this.transform.localPosition = new Vector3(ray.origin.x, ray.origin.y, this.transform.localPosition.z);
    }
}
