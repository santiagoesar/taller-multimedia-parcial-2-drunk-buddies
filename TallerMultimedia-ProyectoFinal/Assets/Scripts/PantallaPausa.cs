﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantallaPausa : MonoBehaviour {

	public void GoToGame(){

		//Busco la pantalla PantallaMundo
		GameObject PantallaPausa = GameObject.Find("Pantallas/PantallaPausa");
		GameObject.Destroy(PantallaPausa);

		// Destruyo la camara que no esta adentro del sobrio
		GameObject otherCamera = GameObject.Find ("CameraOutside");
		otherCamera.SetActive (true);

		GameObject Level = GameObject.Find ("Level");
		Level.SetActive (true);

		GameObject Sobrio = GameObject.Find ("Sobrio");
		Sobrio.SetActive (true);

		GameObject otherPantallaJuego = GameObject.Find("Pantallas/PantallaJuego");
		otherPantallaJuego.SetActive (true);
	}
}