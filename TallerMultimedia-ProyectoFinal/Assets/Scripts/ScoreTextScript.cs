﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTextScript : MonoBehaviour {

	GameManager manager;

	Text Text;
	public static int coinAmount;

	// Use this for initialization
	void Start () {
		Text = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {

		Text.text = coinAmount.ToString ();
		
	}
}
