﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCounter : MonoBehaviour {

	public AudioSource audiocoins;

	GameManager manager;

	GameObject SFXCoin;

	void Start(){
		SFXCoin = GameObject.Find("Sound/SFXCoin");
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player") {
			ScoreTextScript.coinAmount += 25;
			audiocoins.Play ();
			SFXCoin.GetComponent<AudioSource> ().Play ();
			Destroy (gameObject);
		}
	}
}