﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectMoney : MonoBehaviour {

	Vector3 lastPosition;

	GameManager manager;

	void Start () {
		manager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "coin") {
			manager.CoinCollected ();
			Destroy (other.gameObject);
		}
	}

		void update (){

		float diffX = transform.position.x - lastPosition.x;
		float diffY = transform.position.y - lastPosition.y;

		// El personaje secundario se mueve a la derecha
		if (diffX > 0 && Mathf.Abs (diffX) > 0.001) {
			animator.SetBool ("StayIdle", false);
			animator.SetBool ("GoRight", true);

			// El personaje secundario se mueve a la izquierda
		} else if (diffX < 0 && Mathf.Abs (diffX) > 0.001) {
			animator.SetBool ("StayIdle", false);
			animator.SetBool ("GoRight", false);

		} else {
			animator.SetBool ("StayIdle", true);
		}
	}

	public Animator animator;

}

