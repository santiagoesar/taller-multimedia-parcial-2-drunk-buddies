﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamagePlayer : MonoBehaviour {

	public Slider SliderBorrachera;
	public Slider SliderFarra; 

	GameManager manager;

	// Use this for initialization
	void Start () 
	{
		manager = GameObject.Find("GameManager").GetComponent<GameManager> ();

		SliderBorrachera = GameObject.Find ("Pantallas/PantallaJuego/SliderBorrachera/Slider").GetComponent<Slider> ();
		SliderFarra = GameObject.Find ("Pantallas/PantallaJuego/SliderFarra/Slider").GetComponent<Slider> ();
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Enemy") {
			manager.SliderFarra = manager.SliderFarra + 10;
		}
	}


	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Trago") 
		{
			manager.SliderBorrachera = manager.SliderBorrachera - 10;
		}

		if (col.gameObject.tag == "Agua") 
		{
			manager.SliderBorrachera = manager.SliderBorrachera + 10;
			manager.SliderFarra = manager.SliderFarra - 10;
		}
	}
}
	