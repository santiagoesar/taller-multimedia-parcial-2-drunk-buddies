﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaGameOver : MonoBehaviour {

	public GameObject PantallaInicio;

	public void GoToPantallaInicio(){

		//Busco la pantalla PantallaInicio
		GameObject PantallaGameOver = GameObject.Find("Pantallas/PantallaGameOver");
		GameObject.Destroy(PantallaGameOver);

		//Busco al GameObject Pantallas
		GameObject Pantallas = GameObject.Find("Pantallas");
		GameObject pantallaCopia = GameObject.Instantiate(PantallaInicio, Pantallas.transform).gameObject;
		pantallaCopia.name = "PantallaInicio";
	}
}