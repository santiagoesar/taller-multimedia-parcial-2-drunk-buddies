﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaInicio : MonoBehaviour {

	public GameObject PantallaInstrucciones1;

	public void GoToPantallaIntrucciones1(){

		//Busco la pantalla PantallaInicio
		GameObject PantallaInicio = GameObject.Find("Pantallas/PantallaInicio");
		GameObject.Destroy(PantallaInicio);

		//Busco al GameObject Pantallas
		GameObject Pantallas = GameObject.Find("Pantallas");
		GameObject pantallaCopia = GameObject.Instantiate(PantallaInstrucciones1, Pantallas.transform).gameObject;
		pantallaCopia.name = "PantallaInstrucciones1";
	}
}


