﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColisiónSalida : MonoBehaviour {

	public GameObject PantallaWinner;


	void OnCollisionEnter2D(Collision2D col)
{
	if (col.gameObject.tag == "Player") {

			//Busco al GameObject Pantallas
			GameObject Pantallas = GameObject.Find("Pantallas");

			//Creo una copia del prefab Game
			GameObject.Instantiate(PantallaWinner, Pantallas.transform);
			GameObject.Destroy(this);

			// Destruyo la camara que no esta adentro del sobrio
			GameObject otherCamera = GameObject.Find ("CameraOutside");
			otherCamera.SetActive (true);

			// Busco el level para destruirlo
			GameObject level = GameObject.Find("Level");
			GameObject.Destroy (level);
		}
	}
}