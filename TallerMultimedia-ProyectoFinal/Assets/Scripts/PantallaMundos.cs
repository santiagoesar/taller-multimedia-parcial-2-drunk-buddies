﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaMundos : MonoBehaviour {

	public GameObject Level;
	public GameObject Sobrio;
	public GameObject PantallaJuego;
	public GameObject PantallaConfiguracion;
	public GameObject PantallaMenu;

	public void GoToLevel(){

		//Busco la pantalla PantallaMundo
		GameObject PantallaMundos = GameObject.Find("Pantallas/PantallaMundos");
		GameObject.Destroy(PantallaMundos);

		// Muestro la pantalla Juego
		GameObject Pantallas = GameObject.Find("Pantallas");
		GameObject pantallaCopia = GameObject.Instantiate(PantallaJuego, Pantallas.transform).gameObject;
		pantallaCopia.name = "PantallaJuego";

		//Creo una copia del prefab Level
		GameObject levelCopy = GameObject.Instantiate(Level).gameObject;
		levelCopy.name = "Level";

		GameObject sobrioCopy = GameObject.Instantiate(Sobrio).gameObject;
		sobrioCopy.name = "Sobrio";

		// Destruyo la camara que no esta adentro del sobrio
		GameObject otherCamera = GameObject.Find ("CameraOutside");
		otherCamera.SetActive (false);
	}

	public void GoToConfiguration(){

		//Busco la pantalla PantallaMundo
		GameObject PantallaMundos = GameObject.Find("Pantallas/PantallaConfiguracion");
		GameObject.Destroy(PantallaMundos);

		// Muestro la pantalla Juego
		GameObject Pantallas = GameObject.Find("Pantallas");
		GameObject pantallaCopia = GameObject.Instantiate(PantallaConfiguracion, Pantallas.transform).gameObject;
		pantallaCopia.name = "PantallaConfiguracion";

		// Destruyo la camara que no esta adentro del sobrio
		GameObject otherCamera = GameObject.Find ("CameraOutside");
		otherCamera.SetActive (true);
	}

	public void GoToMenu(){

		//Busco la pantalla PantallaMundo
		GameObject PantallaMundos = GameObject.Find("Pantallas/PantallaMundos");
		GameObject.Destroy(PantallaMundos);

		// Muestro la pantalla Juego
		GameObject Pantallas = GameObject.Find("Pantallas");
		GameObject pantallaCopia = GameObject.Instantiate(PantallaMenu, Pantallas.transform).gameObject;
		pantallaCopia.name = "PantallaMenu";

		// Destruyo la camara que no esta adentro del sobrio
		GameObject otherCamera = GameObject.Find ("CameraOutside");
		otherCamera.SetActive (true);
	}

}
	