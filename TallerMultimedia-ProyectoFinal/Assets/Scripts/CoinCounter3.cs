﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCounter3 : MonoBehaviour {

	public AudioSource audiocoins;

	GameObject SFXCoin;

	void Start(){
		SFXCoin = GameObject.Find("Sound/SFXCoin");
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player") {
			ScoreTextScript.coinAmount += 100;
			audiocoins.Play ();
			SFXCoin.GetComponent<AudioSource> ().Play ();
			Destroy (gameObject);
		}
	}
}