﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaInstrucciones3 : MonoBehaviour {

	public GameObject PantallaMundos;

	public void GoToPantallaMundos(){

		//Busco la pantalla PantallaInicio
		GameObject PantallaInstrucciones3 = GameObject.Find("Pantallas/PantallaInstrucciones3");
		GameObject.Destroy(PantallaInstrucciones3);

		//Busco al GameObject Pantallas
		GameObject Pantallas = GameObject.Find("Pantallas");
		GameObject pantallaCopia = GameObject.Instantiate(PantallaMundos, Pantallas.transform).gameObject;
		pantallaCopia.name = "PantallaMundos";
	}
}