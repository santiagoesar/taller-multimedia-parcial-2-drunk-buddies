﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeAnimation : MonoBehaviour {

	Slider SliderBorrachera;
	Slider SliderFarra;

	public Animator animator;


	GameManager manager;

	// Use this for initialization
	void Start () {

		manager = GameObject.Find("GameManager").GetComponent<GameManager> ();

		SliderBorrachera = GameObject.Find ("Pantallas/PantallaJuego/SliderBorrachera/Slider").GetComponent<Slider> ();

		SliderFarra = GameObject.Find ("Pantallas/PantallaJuego/SliderFarra/Slider").GetComponent<Slider> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		
		SliderBorrachera.value = manager.SliderBorrachera;
		SliderFarra.value = manager.SliderFarra;
		
	}

	void Tiempo(){
		
	if (SliderBorrachera.value > 70) {

			animator.SetBool ("StayIdle", false);
			animator.SetBool ("GoRight", true);
	}

	if (SliderBorrachera.value < 30) {
		
			animator.SetBool ("StayIdle", false);
			animator.SetBool ("GoRight", false);

		} else {
			animator.SetBool ("StayIdle", true);
		}
	}

}
