﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaInstrucciones1 : MonoBehaviour {

	public GameObject PantallaInstrucciones2;

	public void GoToPantallaIntrucciones2(){

		//Busco la pantalla PantallaInicio
		GameObject PantallaInstrucciones1 = GameObject.Find("Pantallas/PantallaInstrucciones1");
		GameObject.Destroy(PantallaInstrucciones1);

		//Busco al GameObject Pantallas
		GameObject Pantallas = GameObject.Find("Pantallas");
		GameObject pantallaCopia = GameObject.Instantiate(PantallaInstrucciones2, Pantallas.transform).gameObject;
		pantallaCopia.name = "PantallaInstrucciones2";
	}
}
